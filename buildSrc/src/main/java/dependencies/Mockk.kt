package dependencies

object Mockk {
    val mockk_android = "io.mockk:mockk-android:${Versions.mockk_version}"
}