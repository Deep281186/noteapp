package com.example.notesapplication.business.data

import com.example.notesapplication.business.domain.model.Note
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class NoteDataFactory(private val classLoader: ClassLoader) {


    // getNotesFromFile= fetch data from JSON file in testing environment
    fun getNotesFromFile(fileName: String): String {
        return classLoader.getResource(fileName).readText()
    }

    fun produceHashMapOfNotes(notelist:List<Note>):HashMap<String,Note>{
        val map=HashMap<String,Note>()
        for (note in notelist){
            map.put(note.id.toString(),note)
        }
        return map
    }

    fun produceListOfNotes(): List<Note> {
        val noteList: List<Note> = Gson().fromJson(
            getNotesFromFile("note_list.json"),
            object : TypeToken<List<Note>>() {}.type
        )
        return noteList
    }

    fun produceEmptyListOfNotes(): List<Note>{
        return ArrayList()
    }


}