package com.example.notesapplication.business.interactors.notelist

import com.example.notesapplication.business.data.cache.abstraction.NoteCacheDataSource
import com.example.notesapplication.business.data.util.safeCacheCall
import com.example.notesapplication.business.di.DependencyContainer
import com.example.notesapplication.business.domain.model.NoteFactory
import com.example.notesapplication.business.domain.state.DataState
import com.example.notesapplication.business.domain.state.StateEvent
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListStateEvent
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListViewState
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

/*
Test cases:
1. getNumNotes_success_confirmCorrect()
    a) get the number of notes in cache
    b) listen for GET_NUM_NOTES_SUCCESS from flow emission
    c) compare with the number of notes in the fake data set
*/
class GetNumNotesTest {
    // system in test
    private val getNumNotes: GetNumNotes

    // dependencies
    private val dependencyContainer: DependencyContainer = DependencyContainer()
    private val noteCacheDataSource: NoteCacheDataSource
    private val noteFactory: NoteFactory

    init {
        dependencyContainer.build()
        noteCacheDataSource = dependencyContainer.noteCacheDataSource
        noteFactory = dependencyContainer.noteFactory
        getNumNotes = GetNumNotes(
            noteCacheDataSource = noteCacheDataSource
        )
    }

    @InternalCoroutinesApi
    @Test
    fun getNumNotes_success_confirmCorrect() = runBlocking {

        var numNotes = 0
        getNumNotes.getNumsOfNotes(stateEvent = NoteListStateEvent.GetNumNotesInCacheEvent())
            .collect(object : FlowCollector<DataState<NoteListViewState>?> {
                override suspend fun emit(value: DataState<NoteListViewState>?) {
                    Assertions.assertTrue {
                        value?.stateMessage?.response?.message == GetNumNotes.GET_NUM_NOTES_SUCCESS
                    }
                    Assertions.assertTrue { value?.data?.numNotesInCache!! > numNotes }
                    value?.data?.numNotesInCache?.let {
                        numNotes = it
                    }
                }
            })
        val confirmeNotesCountInCache = noteCacheDataSource.getnumNotes()
        Assertions.assertTrue { confirmeNotesCountInCache == numNotes }
    }

}