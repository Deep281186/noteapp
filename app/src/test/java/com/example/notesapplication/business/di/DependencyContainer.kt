package com.example.notesapplication.business.di

import com.example.notesapplication.business.data.NoteDataFactory
import com.example.notesapplication.business.data.cache.FakeNoteCacheDataSourceImpl
import com.example.notesapplication.business.data.cache.abstraction.NoteCacheDataSource
import com.example.notesapplication.business.data.network.FakeNoteNetworkDataSourceImpl
import com.example.notesapplication.business.data.network.abstraction.NoteNetworkDataSource
import com.example.notesapplication.business.domain.model.Note
import com.example.notesapplication.business.domain.model.NoteFactory
import com.example.notesapplication.business.domain.util.DateUtil
import com.example.notesapplication.util.isUnitTest
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class DependencyContainer {
    private val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH)
    val dateUtil = DateUtil(dateFormat)
    lateinit var noteNetworkDataSource: NoteNetworkDataSource
    lateinit var noteCacheDataSource: NoteCacheDataSource
    lateinit var noteFactory: NoteFactory
    lateinit var noteDataFactory: NoteDataFactory
    private var notesMapData: HashMap<String, Note> = HashMap()
    init {
        isUnitTest = true // for Logger.kt
    }

    fun build() {
        this.javaClass.classLoader?.let {
            noteDataFactory=NoteDataFactory(it)
            notesMapData=noteDataFactory.produceHashMapOfNotes(noteDataFactory.produceListOfNotes())
        }
        noteFactory = NoteFactory(dateUtil)

        noteNetworkDataSource = FakeNoteNetworkDataSourceImpl(
            notesData = notesMapData,
            deletedNotesData = HashMap(),
            dateUtil = dateUtil
        )
        noteCacheDataSource = FakeNoteCacheDataSourceImpl(
            notesData = notesMapData,
            dateUtil = dateUtil
        )
    }
}