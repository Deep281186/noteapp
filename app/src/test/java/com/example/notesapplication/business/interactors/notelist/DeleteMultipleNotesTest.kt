package com.example.notesapplication.business.interactors.notelist

import com.example.notesapplication.business.data.cache.FORCE_NEW_NOTE_EXCEPTION
import com.example.notesapplication.business.data.cache.abstraction.NoteCacheDataSource
import com.example.notesapplication.business.data.network.abstraction.NoteNetworkDataSource
import com.example.notesapplication.business.di.DependencyContainer
import com.example.notesapplication.business.domain.model.Note
import com.example.notesapplication.business.domain.model.NoteFactory
import com.example.notesapplication.business.domain.state.DataState
import com.example.notesapplication.business.interactors.notelist.DeleteMultipleNotes.Companion.DELETE_NOTES_ERRORS
import com.example.notesapplication.business.interactors.notelist.DeleteMultipleNotes.Companion.DELETE_NOTES_SUCCESS
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListStateEvent
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListViewState
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.*
import kotlin.collections.ArrayList

/*
Test cases:
1. deleteNotes_success_confirmNetworkAndCacheUpdated()
    a) select a handful of random notes for deleting
    b) delete from cache and network
    c) confirm DELETE_NOTES_SUCCESS msg is emitted from flow
    d) confirm notes are delted from cache
    e) confirm notes are deleted from "notes" node in network
    f) confirm notes are added to "deletes" node in network
2. deleteNotes_fail_confirmCorrectDeletesMade()
    - This is a complex one:
        - The use-case will attempt to delete all notes passed as input. If there
        is an error with a particular delete, it continues with the others. But the
        resulting msg is DELETE_NOTES_ERRORS. So we need to do rigorous checks here
        to make sure the correct notes were deleted and the correct notes were not.
    a) select a handful of random notes for deleting
    b) change the ids of a few notes so they will cause errors when deleting
    c) confirm DELETE_NOTES_ERRORS msg is emitted from flow
    d) confirm ONLY the valid notes are deleted from network "notes" node
    e) confirm ONLY the valid notes are inserted into network "deletes" node
    f) confirm ONLY the valid notes are deleted from cache
3. throwException_checkGenericError_confirmNetworkAndCacheUnchanged()
    a) select a handful of random notes for deleting
    b) force an exception to be thrown on one of them
    c) confirm DELETE_NOTES_ERRORS msg is emitted from flow
    d) confirm ONLY the valid notes are deleted from network "notes" node
    e) confirm ONLY the valid notes are inserted into network "deletes" node
    f) confirm ONLY the valid notes are deleted from cache
 */
class DeleteMultipleNotesTest {

    // system in test
    private var deleteMultipleNotes: DeleteMultipleNotes? = null

    // dependencies
    private lateinit var dependencyContainer: DependencyContainer
    private lateinit var noteCacheDataSource: NoteCacheDataSource
    private lateinit var noteNetworkDataSource: NoteNetworkDataSource
    private lateinit var noteFactory: NoteFactory


    @BeforeEach
    fun beforeEach() {
        dependencyContainer = DependencyContainer()
        dependencyContainer.build()
        noteCacheDataSource = dependencyContainer.noteCacheDataSource
        noteNetworkDataSource = dependencyContainer.noteNetworkDataSource
        noteFactory = dependencyContainer.noteFactory
        deleteMultipleNotes = DeleteMultipleNotes(
            noteCacheDataSource = noteCacheDataSource,
            noteNetworkDataSource = noteNetworkDataSource
        )
    }

    @AfterEach
    fun afterEach() {
        deleteMultipleNotes = null
    }

    @InternalCoroutinesApi
    @Test
    fun deleteNotes_success_confirmNetworkAndCacheUpdated() = runBlocking {
        var deleteNolist: ArrayList<Note> = ArrayList()
        val noteList = noteCacheDataSource.searchNotes("", "", 10)
        //a) select a handful of random notes for deleting
        for (note in noteList) {
            deleteNolist.add(note)
            if (deleteNolist.size > 5) {
                break
            }
        }
        // b) delete from cache and network
        deleteMultipleNotes?.deleteNotes(
            deleteNolist,
            NoteListStateEvent.DeleteMultipleNotesEvent(deleteNolist)
        )?.collect(object : FlowCollector<DataState<NoteListViewState>?> {
            override suspend fun emit(value: DataState<NoteListViewState>?) {
                // c) confirm DELETE_NOTES_SUCCESS msg is emitted from flow
                Assertions.assertTrue { value?.stateMessage?.response?.message == DELETE_NOTES_SUCCESS }
            }
        })

        //d) confirm notes are delted from cache
        for (note in deleteNolist) {
            val note = noteCacheDataSource.searchNotebyId(note.id)
            Assertions.assertTrue { note == null }
        }

        // e) confirm notes are deleted from "notes" node in network
        val noteNodeList = noteNetworkDataSource.getAllNotes()
        Assertions.assertFalse(noteNodeList.containsAll(deleteNolist))


        // f) confirm notes are added to "deletes" node in network
        val deleteNodeList = noteNetworkDataSource.getDeletedNotes()
        Assertions.assertTrue { deleteNodeList.containsAll(deleteNolist) }
    }

    @InternalCoroutinesApi
    @Test
    fun deleteNotes_fail_confirmCorrectDeletesMade() = runBlocking {

        // a) select a handful of random notes for deleting
        val noteList = noteCacheDataSource.searchNotes("", "", 10)
        val validNotes = ArrayList<Note>()
        val invalidNotes = ArrayList<Note>()
        for (index in 0..noteList.size) {
            if (index % 2 == 0) {
                validNotes.add(noteList.get(index))
            } else {
                // b) change the ids of a few notes so they will cause errors when deleting
                invalidNotes.add(
                    Note(
                        UUID.randomUUID().toString(),
                        UUID.randomUUID().toString(),
                        UUID.randomUUID().toString(),
                        UUID.randomUUID().toString(),
                        UUID.randomUUID().toString()
                    )
                )
            }
            if (validNotes.size + invalidNotes.size > 5) {
                break
            }
        }
        var deleteNolist: ArrayList<Note> = ArrayList(invalidNotes + validNotes)
        deleteMultipleNotes?.deleteNotes(
            deleteNolist,
            NoteListStateEvent.DeleteMultipleNotesEvent(deleteNolist)
        )?.collect(object : FlowCollector<DataState<NoteListViewState>?> {
            override suspend fun emit(value: DataState<NoteListViewState>?) {
                // c) confirm DELETE_NOTES_ERRORS msg is emitted from flow
                Assertions.assertTrue { value?.stateMessage?.response?.message == DELETE_NOTES_ERRORS }
            }
        })

        // d) confirm ONLY the valid notes are deleted from network "notes" node
        val noteNodeList = noteNetworkDataSource.getAllNotes()
        Assertions.assertFalse(noteNodeList.containsAll(validNotes))

        // e) confirm ONLY the valid notes are inserted into network "deletes" node
        val deleteNodeList = noteNetworkDataSource.getDeletedNotes()
        Assertions.assertTrue { deleteNodeList.containsAll(validNotes) }

        // f) confirm ONLY the valid notes are deleted from cache
        for (note in validNotes) {
            val cacheNote = noteCacheDataSource.searchNotebyId(note.id)
            Assertions.assertTrue { cacheNote == null }
        }
    }

    @InternalCoroutinesApi
    @Test
    fun throwException_checkGenericError_confirmNetworkAndCacheUnchanged() = runBlocking {
        //  a) select a handful of random notes for deleting
        val noteList = noteCacheDataSource.searchNotes("", "", 10)
        val validNotes = ArrayList<Note>()
        val invalidNotes = ArrayList<Note>()
        for (note in noteList) {
            validNotes.add(note)
            if (validNotes.size > 5) {
                break
            }
        }
        //  b) force an exception to be thrown on one of them
        invalidNotes.add(
            Note(
                FORCE_NEW_NOTE_EXCEPTION, UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString()
            )
        )
        var deleteNolist: ArrayList<Note> = ArrayList(invalidNotes + validNotes)
        deleteMultipleNotes?.deleteNotes(
            deleteNolist,
            NoteListStateEvent.DeleteMultipleNotesEvent(deleteNolist)
        )?.collect(object : FlowCollector<DataState<NoteListViewState>?> {
            override suspend fun emit(value: DataState<NoteListViewState>?) {
                //  c) confirm DELETE_NOTES_ERRORS msg is emitted from flow
                Assertions.assertTrue { value?.stateMessage?.response?.message == DELETE_NOTES_ERRORS }
            }
        })


        // d) confirm ONLY the valid notes are deleted from network "notes" node
        val noteNodeList = noteNetworkDataSource.getAllNotes()
        Assertions.assertFalse(noteNodeList.containsAll(validNotes))

        // e) confirm ONLY the valid notes are inserted into network "deletes" node
        val deleteNodeList = noteNetworkDataSource.getDeletedNotes()
        Assertions.assertTrue { deleteNodeList.containsAll(validNotes) }

        // f) confirm ONLY the valid notes are deleted from cache
        for (note in validNotes) {
            val cacheNote = noteCacheDataSource.searchNotebyId(note.id)
            Assertions.assertTrue { cacheNote == null }
        }
    }


}