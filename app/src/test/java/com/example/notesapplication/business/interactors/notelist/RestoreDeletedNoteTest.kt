package com.example.notesapplication.business.interactors.notelist

import com.example.notesapplication.business.data.cache.CacheErrors
import com.example.notesapplication.business.data.cache.CacheErrors.CACHE_ERROR_UNKNOWN
import com.example.notesapplication.business.data.cache.FORCE_GENERAL_FAILURE
import com.example.notesapplication.business.data.cache.FORCE_NEW_NOTE_EXCEPTION
import com.example.notesapplication.business.data.cache.abstraction.NoteCacheDataSource
import com.example.notesapplication.business.data.network.abstraction.NoteNetworkDataSource
import com.example.notesapplication.business.di.DependencyContainer
import com.example.notesapplication.business.domain.model.NoteFactory
import com.example.notesapplication.business.domain.state.DataState
import com.example.notesapplication.business.interactors.notelist.RestoreDeletedNote.Companion.RESTORE_NOTE_FAILED
import com.example.notesapplication.business.interactors.notelist.RestoreDeletedNote.Companion.RESTORE_NOTE_SUCCESS
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListStateEvent
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListViewState
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.util.*


/*
Test cases:
1. restoreNote_success_confirmCacheAndNetworkUpdated()
    a) create a new note and insert it into the "deleted" node of network
    b) restore that note
    c) Listen for success msg RESTORE_NOTE_SUCCESS from flow
    d) confirm note is in the cache
    e) confirm note is in the network "notes" node
    f) confirm note is not in the network "deletes" node
2. restoreNote_fail_confirmCacheAndNetworkUnchanged()
    a) create a new note and insert it into the "deleted" node of network
    b) restore that note (force a failure)
    c) Listen for success msg RESTORE_NOTE_FAILED from flow
    d) confirm note is not in the cache
    e) confirm note is not in the network "notes" node
    f) confirm note is in the network "deletes" node
3. throwException_checkGenericError_confirmNetworkAndCacheUnchanged()
    a) create a new note and insert it into the "deleted" node of network
    b) restore that note (force an exception)
    c) Listen for success msg CACHE_ERROR_UNKNOWN from flow
    d) confirm note is not in the cache
    e) confirm note is not in the network "notes" node
    f) confirm note is in the network "deletes" node
 */
class RestoreDeletedNoteTest {


    // system in test
    private val restoreDeletedNote: RestoreDeletedNote

    // dependencies
    private val dependencyContainer: DependencyContainer
    private val noteCacheDataSource: NoteCacheDataSource
    private val noteNetworkDataSource: NoteNetworkDataSource
    private val noteFactory: NoteFactory

    init {
        dependencyContainer = DependencyContainer()
        dependencyContainer.build()
        noteCacheDataSource = dependencyContainer.noteCacheDataSource
        noteNetworkDataSource = dependencyContainer.noteNetworkDataSource
        noteFactory = dependencyContainer.noteFactory
        restoreDeletedNote = RestoreDeletedNote(
            noteCacheDataSource = noteCacheDataSource,
            noteNetworkDataSource = noteNetworkDataSource
        )
    }

    @InternalCoroutinesApi
    @Test
    fun restoreNote_success_confirmCacheAndNetworkUpdated() = runBlocking {
        // a) create a new note and insert it into the "deleted" node of network
        val newNote = noteFactory.createSingleNote(
            id = UUID.randomUUID().toString(),
            title = UUID.randomUUID().toString(),
            body = UUID.randomUUID().toString()
        )
        noteNetworkDataSource.insertDeletedNote(newNote)
        // b) restore that note
        restoreDeletedNote.restoreDeleteNote(
            newNote,
            NoteListStateEvent.RestoreDeletedNoteEvent(note = newNote)
        ).collect(object : FlowCollector<DataState<NoteListViewState>?> {
            override suspend fun emit(value: DataState<NoteListViewState>?) {
                // c) Listen for success msg RESTORE_NOTE_SUCCESS from flow
                Assertions.assertTrue {
                    value?.stateMessage?.response?.message == RESTORE_NOTE_SUCCESS
                }
            }
        })
        // d) confirm note is in the cache
        val cacheNote = noteCacheDataSource.searchNotebyId(newNote.id)
        Assertions.assertTrue { cacheNote == newNote }
        // e) confirm note is in the network "notes" node
        val netWorkNotes = noteNetworkDataSource.searchNote(newNote)
        Assertions.assertTrue { netWorkNotes == newNote }
        // f) confirm note is not in the network "deletes" node
        val deleteNote = noteNetworkDataSource.getDeletedNotes()
        Assertions.assertFalse { deleteNote.contains(newNote) }
    }

    @InternalCoroutinesApi
    @Test
    fun restoreNote_fail_confirmCacheAndNetworkUnchanged() = runBlocking {
      //  a) create a new note and insert it into the "deleted" node of network
        val newNote = noteFactory.createSingleNote(
            id = FORCE_GENERAL_FAILURE,// force insert failure
            title = UUID.randomUUID().toString(),
            body = UUID.randomUUID().toString()
        )
        noteNetworkDataSource.insertDeletedNote(newNote)
        // confirm that note is in the "deletes" node before restoration
        var deletedNotes = noteNetworkDataSource.getDeletedNotes()
        Assertions.assertTrue { deletedNotes.contains(newNote) }
      //  b) restore that note (force a failure)
        restoreDeletedNote.restoreDeleteNote(
            newNote,
            NoteListStateEvent.RestoreDeletedNoteEvent(note = newNote)
        ).collect(object : FlowCollector<DataState<NoteListViewState>?> {
            override suspend fun emit(value: DataState<NoteListViewState>?) {
                //  c) Listen for success msg RESTORE_NOTE_FAILED from flow
                Assertions.assertTrue {
                    value?.stateMessage?.response?.message == RESTORE_NOTE_FAILED
                }
            }
        })
      //  d) confirm note is not in the cache
        val cacheNote = noteCacheDataSource.searchNotebyId(newNote.id)
        Assertions.assertTrue { cacheNote == null }
      //  e) confirm note is not in the network "notes" node
        val netWorkNotes = noteNetworkDataSource.searchNote(newNote)
        Assertions.assertTrue { netWorkNotes == null }
      //  f) confirm note is in the network "deletes" node
        var deletedNotesList = noteNetworkDataSource.getDeletedNotes()
        Assertions.assertTrue { deletedNotesList.contains(newNote) }
    }
    @InternalCoroutinesApi
    @Test
    fun throwException_checkGenericError_confirmNetworkAndCacheUnchanged()= runBlocking {
      //  a) create a new note and insert it into the "deleted" node of network
        val newNote = noteFactory.createSingleNote(
            id = FORCE_NEW_NOTE_EXCEPTION,// force insert failure
            title = UUID.randomUUID().toString(),
            body = UUID.randomUUID().toString()
        )
        noteNetworkDataSource.insertDeletedNote(newNote)
        // confirm that note is in the "deletes" node before restoration
        var deletedNotes = noteNetworkDataSource.getDeletedNotes()
        Assertions.assertTrue { deletedNotes.contains(newNote) }
      //  b) restore that note (force an exception)
        restoreDeletedNote.restoreDeleteNote(
            newNote,
            NoteListStateEvent.RestoreDeletedNoteEvent(note = newNote)
        ).collect(object : FlowCollector<DataState<NoteListViewState>?> {
            override suspend fun emit(value: DataState<NoteListViewState>?) {
                //  c) Listen for success msg CACHE_ERROR_UNKNOWN from flow
             assert(value?.stateMessage?.response?.message!!.contains(CACHE_ERROR_UNKNOWN))
            }
        })

        //  d) confirm note is not in the cache
        val cacheNote = noteCacheDataSource.searchNotebyId(newNote.id)
        Assertions.assertTrue { cacheNote == null }
        //  e) confirm note is not in the network "notes" node
        val netWorkNotes = noteNetworkDataSource.searchNote(newNote)
        Assertions.assertTrue { netWorkNotes == null }
        //  f) confirm note is in the network "deletes" node
        var deletedNotesList = noteNetworkDataSource.getDeletedNotes()
        Assertions.assertTrue { deletedNotesList.contains(newNote) }
    }


}