package com.example.notesapplication.business.interactors.notedetails

import com.example.notesapplication.business.data.cache.CacheErrors
import com.example.notesapplication.business.data.cache.FORCE_UPDATE_NOTE_EXCEPTION
import com.example.notesapplication.business.data.cache.abstraction.NoteCacheDataSource
import com.example.notesapplication.business.data.network.abstraction.NoteNetworkDataSource
import com.example.notesapplication.business.di.DependencyContainer
import com.example.notesapplication.business.domain.model.NoteFactory
import com.example.notesapplication.business.domain.state.DataState
import com.example.notesapplication.business.interactors.notedetails.UpdateNote.Companion.UPDATE_NOTE_FAILED
import com.example.notesapplication.business.interactors.notedetails.UpdateNote.Companion.UPDATE_NOTE_SUCCESS
import com.example.notesapplication.frameWork.presentation.notedetails.state.NoteDetailStateEvent
import com.example.notesapplication.frameWork.presentation.notedetails.state.NoteDetailViewState
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.util.*

/*
Test cases:
1. updateNote_success_confirmNetworkAndCacheUpdated()
    a) select a random note from the cache
    b) update that note
    c) confirm UPDATE_NOTE_SUCCESS msg is emitted from flow
    d) confirm note is updated in network
    e) confirm note is updated in cache
2. updateNote_fail_confirmNetworkAndCacheUnchanged()
    a) attempt to update a note, fail since does not exist
    b) check for failure message from flow emission
    c) confirm nothing was updated in the cache
3. throwException_checkGenericError_confirmNetworkAndCacheUnchanged()
    a) attempt to update a note, force an exception to throw
    b) check for failure message from flow emission
    c) confirm nothing was updated in the cache
 */
class NoteUpdateTest {
    // system in test
    private var noteUpdateNote: UpdateNote

    // dependencies
    private  var dependencyContainer: DependencyContainer=DependencyContainer()
    private lateinit var noteCacheDataSource: NoteCacheDataSource
    private lateinit var noteNetworkDataSource: NoteNetworkDataSource
    private lateinit var noteFactory: NoteFactory

    init {

        dependencyContainer.build()
        noteCacheDataSource = dependencyContainer.noteCacheDataSource
        noteNetworkDataSource = dependencyContainer.noteNetworkDataSource
        noteFactory = dependencyContainer.noteFactory
        noteUpdateNote = UpdateNote(
            noteNetworkDataSource = noteNetworkDataSource,
            noteCacheDataSource = noteCacheDataSource
        )

    }

    @InternalCoroutinesApi
    @Test
    fun updateNote_success_confirmNetworkAndCacheUpdated() = runBlocking {
        //   a) select a random note from the cache
        val note = noteCacheDataSource.searchNotes("", "", 1).get(0)
        // b) update that note
        val updateNote = noteFactory.createSingleNote(
            note.id,
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString()
        )
        noteUpdateNote.updateNote(updateNote, NoteDetailStateEvent.UpdateNoteEvent())
            .collect(object : FlowCollector<DataState<NoteDetailViewState>?> {
                override suspend fun emit(value: DataState<NoteDetailViewState>?) {
                    ///c) confirm UPDATE_NOTE_SUCCESS msg is emitted from flow
                    Assertions.assertTrue { value?.stateMessage?.response?.message == UPDATE_NOTE_SUCCESS }
                }
            })
        // d) confirm note is updated in network
        val newNote = noteNetworkDataSource.searchNote(updateNote)
        Assertions.assertTrue { newNote == updateNote }
        //e) confirm note is updated in cache
        val cacheNote = noteCacheDataSource.searchNotebyId(updateNote.id)
        Assertions.assertTrue { cacheNote == updateNote }
    }

    @InternalCoroutinesApi
    @Test
    fun updateNote_fail_confirmNetworkAndCacheUnchanged() = runBlocking {

        //  a) attempt to update a note, fail since does not exist
        val updateNote = noteFactory.createSingleNote(
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString()
        )
        noteUpdateNote.updateNote(updateNote, NoteDetailStateEvent.UpdateNoteEvent())
            .collect(object : FlowCollector<DataState<NoteDetailViewState>?> {
                override suspend fun emit(value: DataState<NoteDetailViewState>?) {
                    //  b) check for failure message from flow emission
                    Assertions.assertTrue { value?.stateMessage?.response?.message == UPDATE_NOTE_FAILED }
                }
            })
        //  c) confirm nothing was updated in the cache
        val cacheNote = noteCacheDataSource.searchNotebyId(updateNote.id)
        Assertions.assertTrue { cacheNote == null }
        val newNote = noteNetworkDataSource.searchNote(updateNote)
        Assertions.assertTrue { newNote == null }

    }

    @InternalCoroutinesApi
    @Test
    fun throwException_checkGenericError_confirmNetworkAndCacheUnchanged() = runBlocking {
        //   a) attempt to update a note, force an exception to throw
        val updateNote = noteFactory.createSingleNote(
            FORCE_UPDATE_NOTE_EXCEPTION,
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString()
        )
        noteUpdateNote.updateNote(updateNote, NoteDetailStateEvent.UpdateNoteEvent())
            .collect(object : FlowCollector<DataState<NoteDetailViewState>?> {
                override suspend fun emit(value: DataState<NoteDetailViewState>?) {
                    //  b) check for failure message from flow emission
                    assert(value?.stateMessage?.response?.message!!.contains(CacheErrors.CACHE_ERROR_UNKNOWN))
                }
            })

        //  c) confirm nothing was updated in the cache
        val cacheNote = noteCacheDataSource.searchNotebyId(updateNote.id)
        Assertions.assertTrue { cacheNote == null }
        val newNote = noteNetworkDataSource.searchNote(updateNote)
        Assertions.assertTrue { newNote == null }
    }
}