package com.example.notesapplication.business.interactors.notelist

import com.example.notesapplication.business.data.cache.CacheErrors
import com.example.notesapplication.business.data.cache.FORCE_GENERAL_FAILURE
import com.example.notesapplication.business.data.cache.FORCE_NEW_NOTE_EXCEPTION
import com.example.notesapplication.business.data.cache.abstraction.NoteCacheDataSource
import com.example.notesapplication.business.data.network.abstraction.NoteNetworkDataSource
import com.example.notesapplication.business.di.DependencyContainer
import com.example.notesapplication.business.domain.model.NoteFactory
import com.example.notesapplication.business.domain.state.DataState
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListStateEvent
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListViewState
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.ClassRule
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.util.*


/*
Test cases:
1. insertNote_success_confirmNetworkAndCacheUpdated()
    a) insert a new note
    b) listen for INSERT_NOTE_SUCCESS emission from flow
    c) confirm cache was updated with new note
    d) confirm network was updated with new note
2. insertNote_fail_confirmNetworkAndCacheUnchanged()
    a) insert a new note
    b) force a failure (return -1 from db operation)
    c) listen for INSERT_NOTE_FAILED emission from flow
    e) confirm cache was not updated
    e) confirm network was not updated
3. throwException_checkGenericError_confirmNetworkAndCacheUnchanged()
    a) insert a new note
    b) force an exception
    c) listen for CACHE_ERROR_UNKNOWN emission from flow
    e) confirm cache was not updated
    e) confirm network was not updated
 */


class InsertNewNoteTest {
    // system in test
    private val insertNewNote: InsertNewNote

    // dependencies
    private val dependencyContainer: DependencyContainer
    private val noteCacheDataSource: NoteCacheDataSource
    private val noteNetworkDataSource: NoteNetworkDataSource
    private val noteFactory: NoteFactory

    init {
        dependencyContainer = DependencyContainer()
        dependencyContainer.build()
        noteCacheDataSource = dependencyContainer.noteCacheDataSource
        noteNetworkDataSource = dependencyContainer.noteNetworkDataSource
        noteFactory = dependencyContainer.noteFactory
        insertNewNote = InsertNewNote(
            noteCacheDataSource = noteCacheDataSource,
            noteNetworkDataSource = noteNetworkDataSource,
            noteFactory = noteFactory
        )
    }

    @InternalCoroutinesApi
    @Test
    fun insertNote_success_confirmNetworkAndCacheUpdated() = runBlocking {
        val newNote = noteFactory.createSingleNote(
            id = UUID.randomUUID().toString(),
            title = UUID.randomUUID().toString(),
            body = UUID.randomUUID().toString()
        )
        insertNewNote.insertNewNote(
            id = newNote.id,
            title = newNote.title,
          body = newNote.body,
            stateEvent = NoteListStateEvent.InsertNewNoteEvent(
                title = newNote.title
            )
        ).collect(object : FlowCollector<DataState<NoteListViewState>?> {
            override suspend fun emit(value: DataState<NoteListViewState>?) {
                assertEquals(
                    value?.stateMessage?.response?.message,
                    InsertNewNote.INSERT_NOTE_SUCCESS
                )
            }
        })
        // confirm network was updated
        val networkNoteWasInserted = noteNetworkDataSource.searchNote(note = newNote)
        println("networkNoteWasInserted${networkNoteWasInserted.toString()}")
        assertTrue {
            networkNoteWasInserted == newNote
        }
        // confirm cache was updated
        val cacheNoteThatWasInserted = noteCacheDataSource.searchNotebyId(newNote.id)
        assertTrue { cacheNoteThatWasInserted == newNote }
    }

    @InternalCoroutinesApi
    @Test
    fun insertNote_fail_confirmNetworkAndCacheUnchanged() = runBlocking {
        val newNote = noteFactory.createSingleNote(
            id = FORCE_GENERAL_FAILURE,
            title = UUID.randomUUID().toString(),
            body = UUID.randomUUID().toString()
        )
        insertNewNote.insertNewNote(
            id = newNote.id,
            title = newNote.title,
            body = newNote.body,
            stateEvent = NoteListStateEvent.InsertNewNoteEvent(
                title = newNote.title
            )
        ).collect(object : FlowCollector<DataState<NoteListViewState>?> {
            override suspend fun emit(value: DataState<NoteListViewState>?) {
                assertEquals(
                    value?.stateMessage?.response?.message,
                    InsertNewNote.INSERT_NOTE_FAILED
                )
            }
        })
        // confirm network was updated
        val networkNoteWasInserted = noteNetworkDataSource.searchNote(note = newNote)
        println("networkNoteWasInserted${networkNoteWasInserted.toString()}")
        assertTrue {
            networkNoteWasInserted == null
        }
        // confirm cache was updated
        val cacheNoteThatWasInserted = noteCacheDataSource.searchNotebyId(newNote.id)
        assertTrue { cacheNoteThatWasInserted == null }
    }

    @InternalCoroutinesApi
    @Test
    fun throwException_checkGenericError_confirmNetworkAndCacheUnchanged() = runBlocking {
        val newNote = noteFactory.createSingleNote(
            id = FORCE_NEW_NOTE_EXCEPTION,
            title = UUID.randomUUID().toString(),
            body = UUID.randomUUID().toString()
        )
        insertNewNote.insertNewNote(
            id = newNote.id,
            title = newNote.title,
            body = newNote.body,
            stateEvent = NoteListStateEvent.InsertNewNoteEvent(
                title = newNote.title
            )
        ).collect(object : FlowCollector<DataState<NoteListViewState>?> {
            override suspend fun emit(value: DataState<NoteListViewState>?) {
                assertEquals(
                    value?.stateMessage?.response?.message?.contains(CacheErrors.CACHE_ERROR_UNKNOWN),
                    true
                )
            }
        })
        // confirm network was updated
        val networkNoteWasInserted = noteNetworkDataSource.searchNote(note = newNote)
        println("networkNoteWasInserted${networkNoteWasInserted.toString()}")
        assertTrue {
            networkNoteWasInserted == null
        }
        // confirm cache was updated
        val cacheNoteThatWasInserted = noteCacheDataSource.searchNotebyId(newNote.id)
        assertTrue { cacheNoteThatWasInserted == null }
    }
}