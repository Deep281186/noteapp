package com.example.notesapplication.business.interactors.common

import com.example.notesapplication.business.data.cache.CacheErrors
import com.example.notesapplication.business.data.cache.FORCE_DELETE_NOTE_EXCEPTION
import com.example.notesapplication.business.data.cache.abstraction.NoteCacheDataSource
import com.example.notesapplication.business.data.network.abstraction.NoteNetworkDataSource
import com.example.notesapplication.business.di.DependencyContainer
import com.example.notesapplication.business.domain.model.NoteFactory
import com.example.notesapplication.business.domain.state.DataState
import com.example.notesapplication.business.interactors.common.DeleteNote.Companion.DELETE_NOTE_FAILED
import com.example.notesapplication.business.interactors.common.DeleteNote.Companion.DELETE_NOTE_SUCCESS
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListStateEvent
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListViewState
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.util.*


/*
Test cases:
1. deleteNote_success_confirmNetworkUpdated()
    a) delete a note
    b) check for success message from flow emission
    c) confirm note was deleted from "notes" node in network
    d) confirm note was added to "deletes" node in network
2. deleteNote_fail_confirmNetworkUnchanged()
    a) attempt to delete a note, fail since does not exist
    b) check for failure message from flow emission
    c) confirm network was not changed
3. throwException_checkGenericError_confirmNetworkUnchanged()
    a) attempt to delete a note, force an exception to throw
    b) check for failure message from flow emission
    c) confirm network was not changed
 */
class DeleteNoteTest {

    // system in test
    private val deleteNotes: DeleteNote<NoteListViewState>

    // dependencies
    private val dependencyContainer: DependencyContainer
    private val noteCacheDataSource: NoteCacheDataSource
    private val noteNetworkDataSource: NoteNetworkDataSource
    private val noteFactory: NoteFactory

    init {
        dependencyContainer = DependencyContainer()
        dependencyContainer.build()
        noteCacheDataSource = dependencyContainer.noteCacheDataSource
        noteNetworkDataSource = dependencyContainer.noteNetworkDataSource
        noteFactory = dependencyContainer.noteFactory
        deleteNotes = DeleteNote(
            noteCacheDataSource = noteCacheDataSource,
            noteNetWorkDataSource = noteNetworkDataSource
        )
    }

    @InternalCoroutinesApi
    @Test
    fun deleteNote_success_confirmNetworkUpdated() = runBlocking {
        val note = noteCacheDataSource.searchNotes("", "", 1).get(0)
        deleteNotes.deleteNote(note, NoteListStateEvent.DeleteNoteEvent(note))
            .collect(object : FlowCollector<DataState<NoteListViewState>?> {
                override suspend fun emit(value: DataState<NoteListViewState>?) {
                    Assertions.assertEquals(
                        value?.stateMessage?.response?.message,
                        DELETE_NOTE_SUCCESS
                    )

                }
            })

        // confirm note was deleted from "notes" node in network
        val isdeletedFromNetwork = !noteNetworkDataSource.getAllNotes().contains(note)
        Assertions.assertTrue { isdeletedFromNetwork }
        //confirm note was added to "deletes" node in network
        val isdeletedInsertInDeleteNodeNetwork =
            noteNetworkDataSource.getDeletedNotes().contains(note)
        Assertions.assertTrue { isdeletedInsertInDeleteNodeNetwork }
    }

    @InternalCoroutinesApi
    @Test
    fun deleteNote_fail_confirmNetworkUnchanged() = runBlocking {
        val note = noteFactory.createSingleNote(
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString()
        )
        deleteNotes.deleteNote(note, NoteListStateEvent.DeleteNoteEvent(note))
            .collect(object : FlowCollector<DataState<NoteListViewState>?> {
                override suspend fun emit(value: DataState<NoteListViewState>?) {
                    Assertions.assertEquals(
                        value?.stateMessage?.response?.message,
                        DELETE_NOTE_FAILED
                    )

                }
            })

        // confirm note was not deleted from "notes" node in network
        val allNetWorknotes = noteNetworkDataSource.getAllNotes()
        val allCacheNotesNum = noteCacheDataSource.getnumNotes()
        Assertions.assertTrue { allNetWorknotes.size == allCacheNotesNum }
        //confirm note was not added to "deletes" node in network
        val isdeletedInsertInDeleteNode =
            !noteNetworkDataSource.getDeletedNotes().contains(note)
        Assertions.assertTrue { isdeletedInsertInDeleteNode }
    }

    @InternalCoroutinesApi
    @Test
    fun throwException_checkGenericError_confirmNetworkUnchanged() = runBlocking {
        val note = noteFactory.createSingleNote(
            FORCE_DELETE_NOTE_EXCEPTION,
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString()
        )
        deleteNotes.deleteNote(note, NoteListStateEvent.DeleteNoteEvent(note))
            .collect(object : FlowCollector<DataState<NoteListViewState>?> {
                override suspend fun emit(value: DataState<NoteListViewState>?) {
                   assert(
                        value?.stateMessage?.response?.message?.contains(CacheErrors.CACHE_ERROR_UNKNOWN)?:false
                    )

                }
            })

        // confirm note was not deleted from "notes" node in network
        val allNetWorknotes = noteNetworkDataSource.getAllNotes()
        val allCacheNotesNum = noteCacheDataSource.getnumNotes()
        Assertions.assertTrue { allNetWorknotes.size == allCacheNotesNum }
        //confirm note was not added to "deletes" node in network
        val isdeletedInsertInDeleteNode =
            !noteNetworkDataSource.getDeletedNotes().contains(note)
        Assertions.assertTrue { isdeletedInsertInDeleteNode }
    }

}