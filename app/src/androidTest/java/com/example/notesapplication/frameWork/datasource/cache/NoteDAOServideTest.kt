package com.example.notesapplication.frameWork.datasource.cache

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.example.notesapplication.business.data.NoteDataFactoryTest
import com.example.notesapplication.business.domain.model.Note
import com.example.notesapplication.business.domain.util.DateUtil
import com.example.notesapplication.di.TestAppComponent
import com.example.notesapplication.frameWork.datasource.cache.abstraction.NoteDAOService
import com.example.notesapplication.frameWork.datasource.cache.database.NoteDAO
import com.example.notesapplication.frameWork.datasource.cache.implementation.NoteDaoServiceImpl
import com.example.notesapplication.frameWork.datasource.cache.mapper.CacheMapper
import com.example.notesapplication.frameWork.presentation.BaseTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue


/*
    LEGEND:
    1. CBS = "Confirm by searching"
    Test cases:
    1. confirm database note empty to start (should be test data inserted from CacheTest.kt)
    2. insert a new note, CBS
    3. insert a list of notes, CBS
    4. insert 1000 new notes, confirm filtered search query works correctly
    5. insert 1000 new notes, confirm db size increased
    6. delete new note, confirm deleted
    7. delete list of notes, CBS
    8. update a note, confirm updated
    9. search notes, order by date (ASC), confirm order
    10. search notes, order by date (DESC), confirm order
    11. search notes, order by title (ASC), confirm order
    12. search notes, order by title (DESC), confirm order
 */
@FlowPreview
@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class NoteDAOServideTest : BaseTest() {

    //System in test
    private val noteDaoService: NoteDAOService

    //dependencies
    @Inject
    lateinit var noteDao: NoteDAO

    @Inject
    lateinit var noteDataFactoryTest: NoteDataFactoryTest

    @Inject
    lateinit var dateUtils: DateUtil

    @Inject
    lateinit var cacheMapper: CacheMapper

    init {
        injectTest()
        noteDaoService =
            NoteDaoServiceImpl(noteDao = noteDao, dateUtil = dateUtils, noteMapper = cacheMapper)
        insertTestdata()
    }

    fun insertTestdata() = runBlocking {
        /*  val noteEntityList=cacheMapper.noteListToEntityList(noteDataFactoryTest.produceListOfNotes())
          noteDao.insertNotes(noteEntityList)*/
        noteDaoService.insertNotes(noteDataFactoryTest.produceListOfNotes())
    }

    @Test
    fun a_confirmDataBaseNoteEmpty_true() = runBlocking {
        val dataSize = noteDaoService.getNumNotes()
        assertTrue { dataSize > 0 }
    }

    @Test
    fun b_InsertNewNote_true() = runBlocking {
        val newNote = noteDataFactoryTest.createSingleNote(
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString()
        )
        noteDaoService.insertNote(newNote)
        val searchResult1 = noteDaoService.searchNoteById(newNote.id)
        assertTrue { newNote.id == searchResult1?.id }
        val searchResult2 = noteDaoService.searchNotes()
        assertTrue { searchResult2.contains(newNote) }
    }

    @Test
    fun c_InsertNewNoteList_true() = runBlocking {
        val noteList = noteDataFactoryTest.produceListOfNotes()
        noteDaoService.insertNotes(noteList)
        val searchResult = noteDaoService.searchNotes()
        assertTrue { searchResult.containsAll(noteList) }

    }

    @Test
    fun d_Insert1000NewNotes_CheckFilters_True() = runBlocking {

        val new1000Notes = noteDataFactoryTest.createNumberOfNotes(1000)
        noteDaoService.insertNotes(new1000Notes)

        repeat(50) {
            val randomIndex = Random.nextInt(0, new1000Notes.size - 1)
            val searchResult = noteDaoService.searchNotesOrderByTitleASC(
                query = new1000Notes.get(randomIndex).title,
                page = 1,
                pageSize = 1
            )
            assertEquals(new1000Notes.get(randomIndex).title, searchResult.get(0).title)
        }
    }

    @Test
    fun e_Insert1000NewNotes_CheckDBSize_True() = runBlocking {
        val dataSize = noteDaoService.getNumNotes()
        val new1000Notes = noteDataFactoryTest.createNumberOfNotes(1000)
        noteDaoService.insertNotes(new1000Notes)
        val updateddataSize = noteDaoService.getNumNotes()
        assertEquals(dataSize + 1000, updateddataSize)
    }

    @Test
    fun f_DeleteNewNote_ConfirmDelete_True() = runBlocking {
        val data=noteDaoService.getAllNotes()
        val randomIndex = Random.nextInt(0, data.size-1)
        noteDaoService.deleteNote(data.get(randomIndex).id)

        val searchResult=noteDaoService.searchNoteById(data.get(randomIndex).id)
        assertTrue { searchResult==null }

    }

    @Test
    fun g_DeleteListOfNotes_ConfirmDelete_True() = runBlocking {
        val data=ArrayList(noteDaoService.getAllNotes())
        val notesToDelete=ArrayList<Note>()
        repeat(5)
        {
            val randomIndex = Random.nextInt(0, data.size-1)
            noteDaoService.searchNoteById(data.get(randomIndex).id)?.let {
                notesToDelete.add(it)
                data.removeAt(randomIndex)
            }

        }

        noteDaoService.deleteNotes(notesToDelete)
        val searchResult=noteDaoService.getAllNotes()
        assertFalse { searchResult.containsAll(notesToDelete) }
    }

    @Test
    fun h_updateNote_ConfirmUpdate_True() = runBlocking {
        val data=noteDaoService.getAllNotes()
        val randomIndex = Random.nextInt(0, data.size-1)
        var searchResult=noteDaoService.searchNoteById(data.get(randomIndex).id)
            assertTrue { searchResult!=null }

        noteDaoService.updateNote(searchResult!!.id,"Updated Title","Update Body",null)
        val checkNoteUpdate=noteDaoService.searchNoteById(searchResult.id)
        assertEquals(checkNoteUpdate?.title,"Updated Title")
    }

    @Test
    fun searchNotes_orderByDateASC_confirmOrder() = runBlocking {
        val noteList = noteDaoService.searchNotesOrderByDateASC(
            query = "",
            page = 1,
            pageSize = 100
        )

        // check that the date gets larger (newer) as iterate down the list
        var previousNoteDate = noteList.get(0).updated_at
        for(index in 1..noteList.size - 1){
            val currentNoteDate = noteList.get(index).updated_at
            assertTrue { currentNoteDate >= previousNoteDate }
            previousNoteDate = currentNoteDate
        }
    }


    @Test
    fun searchNotes_orderByDateDESC_confirmOrder() = runBlocking {
        val noteList = noteDaoService.searchNotesOrderByDateDESC(
            query = "",
            page = 1,
            pageSize = 100
        )

        // check that the date gets larger (newer) as iterate down the list
        var previous = noteList.get(0).updated_at
        for(index in 1..noteList.size - 1){
            val current = noteList.get(index).updated_at
            assertTrue { current <= previous }
            previous = current
        }
    }

    @Test
    fun searchNotes_orderByTitleASC_confirmOrder() = runBlocking {
        val noteList = noteDaoService.searchNotesOrderByTitleASC(
            query = "",
            page = 1,
            pageSize = 100
        )

        // check that the date gets larger (newer) as iterate down the list
        var previous = noteList.get(0).title
        for(index in 1..noteList.size - 1){
            val current = noteList.get(index).title

            assertTrue {
                listOf(previous, current)
                    .asSequence()
                    .zipWithNext { a, b ->
                        a <= b
                    }.all { it }
            }
            previous = current
        }
    }

    @Test
    fun searchNotes_orderByTitleDESC_confirmOrder() = runBlocking {
        val noteList = noteDaoService.searchNotesOrderByTitleDESC(
            query = "",
            page = 1,
            pageSize = 100
        )

        // check that the date gets larger (newer) as iterate down the list
        var previous = noteList.get(0).title
        for(index in 1..noteList.size - 1){
            val current = noteList.get(index).title

            assertTrue {
                listOf(previous, current)
                    .asSequence()
                    .zipWithNext { a, b ->
                        a >= b
                    }.all { it }
            }
            previous = current
        }
    }

    override fun injectTest() {
        (application.appComponent as TestAppComponent).inject(this)
    }
}