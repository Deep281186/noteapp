package com.example.notesapplication.frameWork.presentation

import com.example.notesapplication.di.DaggerTestAppComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview


@FlowPreview
@ExperimentalCoroutinesApi
class TestBaseApplication:BaseApplication() {

    override fun initAppComponent() {
        appComponent = DaggerTestAppComponent.factory().create(this)
    }
}