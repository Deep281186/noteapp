package com.example.notesapplication.frameWork.presentation

import androidx.test.core.app.ApplicationProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@FlowPreview
@ExperimentalCoroutinesApi
abstract class BaseTest {
    val application: TestBaseApplication =
        ApplicationProvider.getApplicationContext() as TestBaseApplication
    abstract fun injectTest()
}