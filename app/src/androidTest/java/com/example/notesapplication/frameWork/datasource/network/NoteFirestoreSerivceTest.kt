package com.example.notesapplication.frameWork.datasource.network

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.example.notesapplication.business.data.NoteDataFactoryTest
import com.example.notesapplication.business.domain.model.Note
import com.example.notesapplication.di.TestAppComponent
import com.example.notesapplication.frameWork.datasource.network.abstraction.NoteFirestoreService
import com.example.notesapplication.frameWork.datasource.network.implementation.NoteFirestoreServiceImpl
import com.example.notesapplication.frameWork.datasource.network.mapper.NetworkMapper
import com.example.notesapplication.frameWork.presentation.BaseTest
import com.example.notesapplication.util.printLogD
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.tasks.await
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*
import javax.inject.Inject
import kotlin.random.Random.Default
import kotlin.random.Random.Default.nextInt
import kotlin.test.*

@FlowPreview
@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class NoteFirestoreSerivceTest : BaseTest() {

    lateinit var noteFirestoreService: NoteFirestoreService


    @Inject
    lateinit var firebaseFirestore: FirebaseFirestore

    @Inject
    lateinit var firebaseAuth: FirebaseAuth

    @Inject
    lateinit var noteFactory: NoteDataFactoryTest

    @Inject
    lateinit var networkMapper: NetworkMapper

    init {
        injectTest()
        signup()
        //
    }

    override fun injectTest() {
        (application.appComponent as TestAppComponent).inject(this)
    }

    @Before
    fun before() {
        noteFirestoreService = NoteFirestoreServiceImpl(
            firebaseAuth = firebaseAuth,
            fireStore = firebaseFirestore,
            networkMapper = networkMapper
        )
        //signup()

    }

    @Test
    fun a_insertSingleNote_CBS() = runBlocking {
        val note = noteFactory.createSingleNote(
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString()
        )
        noteFirestoreService.insertOrUpdateNote(note)
        val searchedNote = noteFirestoreService.searchNote(note)
        insertNoteList()
        assertEquals(note, searchedNote)

    }


    private fun insertNoteList() = runBlocking {
        val noteList = noteFactory.createNumberOfNotes(5)

        for (note in noteList) {
            printLogD(this.javaClass.simpleName, "--->" + noteList.size)
            noteFirestoreService.insertOrUpdateNote(note)
        }
    }

    /*  @Test
      fun c_getAllNotes() = runBlocking {
          val noteList = noteFirestoreService.getAllNotes()
          printLogD(this.javaClass.simpleName,"--->"+noteList.size)
          assertEquals(noteList.size, 12)
      }*/
    @Test
    fun updateSingleNote_CBS() = runBlocking {
        val note = noteFactory.createSingleNote(
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString()
        )
        noteFirestoreService.insertOrUpdateNote(note)
        val alNotes = noteFirestoreService.getAllNotes()
        val sizeafterInsert = alNotes.size;
        printLogD(this.javaClass.simpleName, "sizeafterInsert--->" + sizeafterInsert)
        val searchNote = noteFirestoreService.searchNote(note)!!
        val updatedNote = noteFactory.updateNote(
            searchNote.id,
            "Updated Title",
            "Updated Body",
            searchNote.created_at
        )
        noteFirestoreService.insertOrUpdateNote(updatedNote)
        val alNotesnew = noteFirestoreService.getAllNotes()
        val sizeafterUpdate = alNotesnew.size;
        printLogD(this.javaClass.simpleName, "sizeafterUpdate--->" + sizeafterUpdate)
        assertEquals(sizeafterInsert, sizeafterUpdate)
        val checkNote = noteFirestoreService.searchNote(updatedNote)!!
        printLogD(this.javaClass.simpleName, "--->" + checkNote.title)
        assertEquals(checkNote.title, "Updated Title")

    }

    @Test

    fun updateRandomSingleNote_CBS() = runBlocking {
        val alNotes = noteFirestoreService.getAllNotes()

        val note = alNotes.get(alNotes.size-1)
        val updatedNote = noteFactory.updateNote(
            note.id,
            "Updated Title:updateRandomSingleNote_CBS",
            "Updated Body:updateRandomSingleNote_CBS",
            note.created_at
        )
        noteFirestoreService.insertOrUpdateNote(updatedNote)
        val checkNote = noteFirestoreService.searchNote(updatedNote)!!
        assertEquals(checkNote.title, "Updated Title:updateRandomSingleNote_CBS")
        assertEquals(checkNote.body, "Updated Body:updateRandomSingleNote_CBS")
    }

    @Test
    fun deleteRandomSingleNote_CBS() = runBlocking {
        val alNotes = noteFirestoreService.getAllNotes()
        val size = alNotes.size
        val note = alNotes.get(size-1)
        noteFirestoreService.deleteNote(note.id)
        val checkAllNote = noteFirestoreService.getAllNotes()
        val updatedSize = checkAllNote.size
        val checkNote = noteFirestoreService.searchNote(note)
        assertTrue { checkNote == null }
        assertNotEquals(size, updatedSize)
    }

    @Test
    fun insertRandomNoteInDeleteNode_CBS() = runBlocking {
        val alNotes = noteFirestoreService.getAllNotes()
        val size = alNotes.size
        val deletenote = alNotes.get(size-1)
        noteFirestoreService.deleteNote(deletenote.id)
        val checkAllNote = noteFirestoreService.getAllNotes()
        val updatedSize = checkAllNote.size
        val checkNote = noteFirestoreService.searchNote(deletenote)
        assertTrue { checkNote == null }
        assertNotEquals(size, updatedSize)

        noteFirestoreService.insertDeletedNote(deletenote)

        val checkDeletedNote = noteFirestoreService.getDeletedNotes()

        assertTrue { checkDeletedNote.contains(deletenote) }


    }
    @Test
    fun insertListIntoDeletesNode_CBS() = runBlocking {
        val noteList = ArrayList(noteFirestoreService.getAllNotes())

        // choose some random notes to add to "deletes" node
        val notesToDelete: ArrayList<Note> = ArrayList()

        // 1st
        var noteToDelete = noteList.get(nextInt(0, noteList.size - 1) + 1)
        noteList.remove(noteToDelete)
        notesToDelete.add(noteToDelete)

        // 2nd
        noteToDelete = noteList.get(nextInt(0, noteList.size - 1) + 1)
        noteList.remove(noteToDelete)
        notesToDelete.add(noteToDelete)

        // 3rd
        noteToDelete = noteList.get(nextInt(0, noteList.size - 1) + 1)
        noteList.remove(noteToDelete)
        notesToDelete.add(noteToDelete)

        // 4th
        noteToDelete = noteList.get(nextInt(0, noteList.size - 1) + 1)
        noteList.remove(noteToDelete)
        notesToDelete.add(noteToDelete)

        // insert into "deletes" node
        noteFirestoreService
            .insertDeletedNotes(notesToDelete)

        // confirm the notes are in "deletes" node
        val searchResults = noteFirestoreService.getDeletedNotes()

        assertTrue { searchResults.containsAll(notesToDelete) }
    }

    @Test
    fun deleteDeletedNote_CBS() = runBlocking {
        val note = noteFactory.createSingleNote(
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString()
        )

        // insert into "deletes" node
        noteFirestoreService.insertDeletedNote(note)

        // confirm note is in "deletes" node
        var searchResults = noteFirestoreService.getDeletedNotes()

        assertTrue { searchResults.contains(note) }

        // delete from "deletes" node
        noteFirestoreService.deleteDeletedNote(note)

        // confirm note is deleted from "deletes" node
        searchResults = noteFirestoreService.getDeletedNotes()

        assertFalse { searchResults.contains(note) }
    }

    private fun signup() = runBlocking {

        firebaseAuth.signInWithEmailAndPassword(EMAIL, PASSWORD).await()
    }

    companion object {
        const val EMAIL = "deep281186@gmail.com"
        const val PASSWORD = "password"
        const val TEST="TEST"
    }

}