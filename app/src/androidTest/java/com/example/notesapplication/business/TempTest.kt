package com.example.notesapplication.business

import androidx.test.core.app.ApplicationProvider
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.example.notesapplication.di.TestAppComponent
import com.example.notesapplication.frameWork.presentation.TestBaseApplication
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

@FlowPreview
@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class TempTest {

    val application:TestBaseApplication = ApplicationProvider.getApplicationContext() as TestBaseApplication

    @Inject
    lateinit var firebaseFirestore: FirebaseFirestore

    init {
       // (application.appComponent as TestAppComponent).inject(this)
    }

    @Test
    fun checkFireBaseDependency(){
        assert(::firebaseFirestore.isInitialized)
    }

}