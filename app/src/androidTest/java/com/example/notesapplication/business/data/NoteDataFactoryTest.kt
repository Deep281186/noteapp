package com.example.notesapplication.business.data

import android.app.Application
import android.content.res.AssetManager
import com.example.notesapplication.business.domain.model.Note
import com.example.notesapplication.business.domain.model.NoteFactory
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.IOException
import java.io.InputStream
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NoteDataFactoryTest @Inject() constructor(
    private val application: Application,
    private val noteFactory: NoteFactory
) {
    private fun readDataFromJSON(filename: String): String? {
        var json: String? = null
        json = try {
            val inputStream: InputStream = (application.assets as AssetManager).open(filename)
            inputStream.bufferedReader().use { it.readText() }
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json

    }

    fun createSingleNote(id: String, title: String, body: String) =
        noteFactory.createSingleNote(id = id, body = body, title = title)

    fun createNumberOfNotes(num: Int) = noteFactory.createNoteList(num)

    fun updateNote(
        id: String,
        title: String,
        body: String? = null,
        createAt: String
    ) = noteFactory.updateNote(id = id, title = title, body = body, createAt = createAt)

    fun produceHashMapOfNotes(notelist: List<Note>): HashMap<String, Note> {
        val map = HashMap<String, Note>()
        for (note in notelist) {
            map.put(note.id.toString(), note)
        }
        return map
    }

    fun produceListOfNotes(): List<Note> {
        val noteList: List<Note> = Gson().fromJson(
            readDataFromJSON("note_list.json"),
            object : TypeToken<List<Note>>() {}.type
        )
        return noteList
    }


    fun produceEmptyListOfNotes(): List<Note> {
        return ArrayList()
    }

}