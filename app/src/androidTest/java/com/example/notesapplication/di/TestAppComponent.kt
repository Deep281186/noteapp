package com.example.notesapplication.di

import com.example.notesapplication.business.TempTest
import com.example.notesapplication.frameWork.datasource.cache.NoteDAOServideTest
import com.example.notesapplication.frameWork.datasource.network.NoteFirestoreSerivceTest
import com.example.notesapplication.frameWork.presentation.TestBaseApplication
import dagger.BindsInstance
import dagger.Component
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Singleton

@FlowPreview
@ExperimentalCoroutinesApi
@Singleton
@Component(modules = [AppModule::class,TestModule::class])
interface TestAppComponent : AppComponent {

    @Component.Factory
    interface Factory{
        fun create(@BindsInstance app:TestBaseApplication):TestAppComponent
    }

    //fun inject(tempTest: TempTest)
    fun inject(noteFirestoreSerivceTest: NoteFirestoreSerivceTest)

    fun inject(noteDAOServideTest: NoteDAOServideTest)
}