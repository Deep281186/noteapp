package com.example.notesapplication.di

import androidx.room.Room
import com.example.notesapplication.business.data.NoteDataFactoryTest
import com.example.notesapplication.business.domain.model.NoteFactory
import com.example.notesapplication.business.domain.util.DateUtil
import com.example.notesapplication.business.domain.util.DateUtil_Factory
import com.example.notesapplication.frameWork.datasource.cache.database.NoteDatabase
import com.example.notesapplication.frameWork.presentation.TestBaseApplication
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Singleton

@FlowPreview
@ExperimentalCoroutinesApi
@Module
object TestModule {

    @JvmStatic
    @Singleton
    @Provides
    fun provideNoteDb(app: TestBaseApplication): NoteDatabase {
        return Room
            .inMemoryDatabaseBuilder(app, NoteDatabase::class.java)
            .fallbackToDestructiveMigration()
            .build()
    }

   /* @JvmStatic
    @Singleton
    @Provides
    fun provideFirebaseFirestore(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }*/


/*    @JvmStatic
    @Singleton
    @Provides
    fun provideDateUtils():DateUtil{
        return DateUtil(SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH))
    }
    @JvmStatic
    @Singleton
    @Provides
    fun provideNoteFactory(dateUtil: DateUtil):NoteFactory{
        return NoteFactory(dateUtil)
    }*/

    @JvmStatic
    @Singleton
    @Provides
    fun provideFireBaseSettings(): FirebaseFirestoreSettings {
        return FirebaseFirestoreSettings.Builder().setHost("10.0.2.2:8080").setSslEnabled(false)
            .setPersistenceEnabled(false).build()
    }

    @JvmStatic
    @Singleton
    @Provides
    fun provideFireBaseFireStore(settings:FirebaseFirestoreSettings):FirebaseFirestore{
        val firestore=FirebaseFirestore.getInstance()
        firestore.firestoreSettings=settings
        return firestore
    }
    @JvmStatic
    @Singleton
    @Provides
    fun provideNoteDataFactory(app: TestBaseApplication,noteFactory: NoteFactory):NoteDataFactoryTest{
        return NoteDataFactoryTest(app,noteFactory)
    }
}