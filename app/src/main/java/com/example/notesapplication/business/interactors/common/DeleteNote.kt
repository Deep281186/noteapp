package com.example.notesapplication.business.interactors.common

import com.example.notesapplication.business.data.cache.CacheResponseHandler
import com.example.notesapplication.business.data.cache.abstraction.NoteCacheDataSource
import com.example.notesapplication.business.data.network.abstraction.NoteNetworkDataSource
import com.example.notesapplication.business.data.util.safeCacheCall
import com.example.notesapplication.business.domain.model.Note
import com.example.notesapplication.business.domain.state.*
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListStateEvent
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.flow

class DeleteNote<ViewState> constructor(
    val noteNetWorkDataSource: NoteNetworkDataSource,
    val noteCacheDataSource: NoteCacheDataSource

) {

    fun deleteNote(note: Note, stateEvent: StateEvent): Flow<DataState<ViewState>?> =
        flow {
            val cacheReponse = safeCacheCall(IO) {
                noteCacheDataSource.deleteNote(note.id)
            }
            val cacheResult =
                object : CacheResponseHandler<ViewState, Int>(cacheReponse, stateEvent) {
                    override suspend fun handleSuccess(resultObj: Int): DataState<ViewState>? {
                        return if (resultObj > 0) {
                            DataState.data(
                                response = Response(
                                    message = DELETE_NOTE_SUCCESS,
                                    uiComponentType = UIComponentType.None(),
                                    messageType = MessageType.Success()
                                ),
                                data = null,
                                stateEvent = stateEvent
                            )
                        } else {
                            DataState.data(
                                response = Response(
                                    message = DELETE_NOTE_FAILED,
                                    uiComponentType = UIComponentType.Toast(),
                                    messageType = MessageType.Error()
                                ),
                                data = null,
                                stateEvent = stateEvent
                            )
                        }
                    }
                }.getResult()
            emit(cacheResult)
            cacheResult?.stateMessage?.response?.message?.let {
                updateNetwork(it, note)
            }

        }

    private suspend fun updateNetwork(message: String, note: Note) {
        if (message.equals(DELETE_NOTE_SUCCESS)) {
            // delete from 'notes' node
            safeCacheCall(IO) { noteNetWorkDataSource.deleteNote(note.id) }
            // insert into 'deletes' node
            safeCacheCall(IO) { noteNetWorkDataSource.insertDeletedNote(note) }
        }
    }

    companion object {
        val DELETE_NOTE_SUCCESS = "Successfully deleted note."
        val DELETE_NOTE_PENDING = "Delete pending..."
        val DELETE_NOTE_FAILED = "Failed to delete note."
        val DELETE_ARE_YOU_SURE = "Are you sure you want to delete this?"
    }
}