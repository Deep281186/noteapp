package com.example.notesapplication.business.interactors.notelist

import com.example.notesapplication.business.interactors.common.DeleteNote
import com.example.notesapplication.business.interactors.notedetails.UpdateNote
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListViewState
import javax.inject.Inject

class NoteDetailInteractors @Inject constructor(
    private val deleteNote: DeleteNote<NoteListViewState>,
    private val updateNote: UpdateNote
) {
}