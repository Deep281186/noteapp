package com.example.notesapplication.business.interactors.notelist

import com.example.notesapplication.business.data.cache.CacheResponseHandler
import com.example.notesapplication.business.data.cache.abstraction.NoteCacheDataSource
import com.example.notesapplication.business.data.network.abstraction.NoteNetworkDataSource
import com.example.notesapplication.business.data.util.safeApiCall
import com.example.notesapplication.business.data.util.safeCacheCall
import com.example.notesapplication.business.domain.model.Note
import com.example.notesapplication.business.domain.model.NoteFactory
import com.example.notesapplication.business.domain.state.*
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListViewState
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.util.*

class InsertNewNote(
    private val noteCacheDataSource: NoteCacheDataSource,
    private val noteNetworkDataSource: NoteNetworkDataSource,
    private val noteFactory: NoteFactory
) {

    fun insertNewNote(
        id: String? = null,
        title: String,
        body: String,
        stateEvent: StateEvent
    ): Flow<DataState<NoteListViewState>?> = flow {


        val newNote =
            noteFactory.createSingleNote(id ?: UUID.randomUUID().toString(), title,body)
        val cachResults = safeCacheCall(IO) {
            noteCacheDataSource.insertNote(newNote)
        }
        // val caceResult =
        val cacheResponse = object : CacheResponseHandler<NoteListViewState, Long>(
            response = cachResults,
            stateEvent = stateEvent
        ) {
            override suspend fun handleSuccess(resultObj: Long): DataState<NoteListViewState>? {
                return if (resultObj > 0) {
                    val viewState =
                        NoteListViewState(
                            newNote = newNote
                        )
                    DataState.data(
                        response = Response(
                            INSERT_NOTE_SUCCESS,
                            UIComponentType.Toast(),
                            MessageType.Success()
                        ), data = viewState, stateEvent = stateEvent
                    )
                } else {
                    DataState.data(
                        response = Response(
                            INSERT_NOTE_FAILED,
                            UIComponentType.Toast(),
                            MessageType.Error()
                        ), data = null, stateEvent = stateEvent
                    )
                }
            }
        }.getResult()
        emit(cacheResponse)
        updateNetwork(cacheResponse?.stateMessage?.response?.message, newNote)
    }
    //  var cacheResponse: DataState<NoteListViewState>? = null


    private suspend fun updateNetwork(cacheResponse: String?, newNote: Note) {
        if (cacheResponse.equals(INSERT_NOTE_SUCCESS)) {
            safeApiCall(IO){
                noteNetworkDataSource.insertOrUpdateNote(note = newNote)
            }

        }

    }

    companion object {
        val INSERT_NOTE_SUCCESS = "Successfully inserted new note."
        val INSERT_NOTE_FAILED = "Failed to insert new note."
    }
}