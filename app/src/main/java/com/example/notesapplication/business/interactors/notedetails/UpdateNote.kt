package com.example.notesapplication.business.interactors.notedetails

import com.example.notesapplication.business.data.cache.CacheResponseHandler
import com.example.notesapplication.business.data.cache.abstraction.NoteCacheDataSource
import com.example.notesapplication.business.data.network.abstraction.NoteNetworkDataSource
import com.example.notesapplication.business.data.util.safeApiCall
import com.example.notesapplication.business.data.util.safeCacheCall
import com.example.notesapplication.business.domain.model.Note
import com.example.notesapplication.business.domain.state.*
import com.example.notesapplication.frameWork.presentation.notedetails.state.NoteDetailViewState
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListViewState
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class UpdateNote(
    val noteNetworkDataSource: NoteNetworkDataSource,
    val noteCacheDataSource: NoteCacheDataSource
) {


    fun updateNote(
        updatedNote: Note,
        stateEvent: StateEvent
    ): Flow<DataState<NoteDetailViewState>?> = flow {

        val cacheResponse = safeCacheCall(IO) {
            noteCacheDataSource.updateNote(updatedNote.id, updatedNote.title, updatedNote.body,timeStamp = null)
        }
        val cacehResult =
            object : CacheResponseHandler<NoteDetailViewState, Int>(cacheResponse, stateEvent) {
                override suspend fun handleSuccess(resultObj: Int): DataState<NoteDetailViewState>? {
                    return if (resultObj > 0) {
                        DataState.data(
                            response = Response(
                                message = UPDATE_NOTE_SUCCESS,
                                uiComponentType = UIComponentType.Toast(),
                                messageType = MessageType.Success()
                            ),
                            data = null,
                            stateEvent = stateEvent
                        )
                    } else {
                        DataState.data(
                            response = Response(
                                message = UPDATE_NOTE_FAILED,
                                uiComponentType = UIComponentType.Toast(),
                                messageType = MessageType.Error()
                            ),
                            data = null,
                            stateEvent = stateEvent
                        )
                    }
                }
            }.getResult()

        emit(cacehResult)
        cacehResult?.stateMessage?.response?.message?.let {
            updateNetwork(it,updatedNote)
        }
    }

    private suspend fun updateNetwork(message: String, note: Note) {
        if(message.equals(UPDATE_NOTE_SUCCESS)){
            safeApiCall(IO) {
                noteNetworkDataSource.insertOrUpdateNote(note)
            }
        }

    }

    companion object {
        val UPDATE_NOTE_SUCCESS = "Successfully updated note."
        val UPDATE_NOTE_FAILED = "Failed to update note."
        val UPDATE_NOTE_FAILED_PK = "Update failed. Note is missing primary key."

    }

}