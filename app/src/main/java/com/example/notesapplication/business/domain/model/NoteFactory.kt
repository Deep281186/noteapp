package com.example.notesapplication.business.domain.model

import com.example.notesapplication.business.domain.util.DateUtil
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.ArrayList

@Singleton
class NoteFactory @Inject constructor(private val dateUtil: DateUtil) {

    fun createSingleNote(
        id: String? = null,
        title: String,
        body: String? = null
    ): Note {
        return Note(
            id = id ?: UUID.randomUUID().toString(),
            title = title,
            body = body ?: "",
            created_at = dateUtil.getCurrentTimeStamp(),
            updated_at = dateUtil.getCurrentTimeStamp()
        )
    }
    fun updateNote(
        id: String,
        title: String,
        body: String? = null,
        createAt:String
    ): Note {
        return Note(
            id = id,
            title = title,
            body = body ?: "",
            created_at = createAt,
            updated_at = dateUtil.getCurrentTimeStamp()
        )
    }
    //For testing only
    fun createNoteList(numNote: Int): List<Note> {
        val list = ArrayList<Note>()
        for (i in 0 until numNote) {
            list.add(
                createSingleNote(
                    id = null,
                    title = UUID.randomUUID().toString(),
                    body = UUID.randomUUID().toString()
                )
            )
        }
        return list
    }
}