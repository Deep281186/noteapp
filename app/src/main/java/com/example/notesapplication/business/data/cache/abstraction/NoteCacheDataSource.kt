package com.example.notesapplication.business.data.cache.abstraction

import com.example.notesapplication.business.domain.model.Note

interface NoteCacheDataSource {

    suspend fun insertNote(note: Note): Long
    suspend fun deleteNote(pk: String): Int
    suspend fun deleteNotes(note: List<Note>): Int
    suspend fun updateNote(pk: String, newTitle: String, body: String?,timeStamp:String?): Int
    suspend fun searchNotes(query: String, filterAndOrder: String, page: Int): List<Note>
    suspend fun searchNotebyId(pk: String): Note?
    suspend fun getAllNotes(): List<Note>
    suspend fun getnumNotes(): Int
    suspend fun insertNotes(note: List<Note>): LongArray
}