package com.example.notesapplication.business.interactors.notelist

import com.example.notesapplication.business.data.cache.CacheResponseHandler
import com.example.notesapplication.business.data.cache.abstraction.NoteCacheDataSource
import com.example.notesapplication.business.data.network.abstraction.NoteNetworkDataSource
import com.example.notesapplication.business.data.util.safeApiCall
import com.example.notesapplication.business.data.util.safeCacheCall
import com.example.notesapplication.business.domain.model.Note
import com.example.notesapplication.business.domain.state.*
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListViewState
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListViewState.NotePendingDelete
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class RestoreDeletedNote(

    val noteCacheDataSource: NoteCacheDataSource,
    val noteNetworkDataSource: NoteNetworkDataSource
) {
    fun restoreDeleteNote(note: Note, stateEvent: StateEvent): Flow<DataState<NoteListViewState>?> =
        flow {
            val cacheResponse = safeCacheCall(IO) {
                noteCacheDataSource.insertNote(note)
            }
            val cacheResult = object : CacheResponseHandler<NoteListViewState, Long>(
                response = cacheResponse,
                stateEvent = stateEvent
            ) {
                override suspend fun handleSuccess(resultObj: Long): DataState<NoteListViewState>? {
                    return if (resultObj > 0) {
                        val viewState =
                            NoteListViewState(
                                notePendingDelete = NotePendingDelete(note = note)
                            )
                        DataState.data(
                            response = Response(
                                RESTORE_NOTE_SUCCESS
                                ,
                                UIComponentType.Toast(),
                                MessageType.Success()
                            ), data = viewState, stateEvent = stateEvent
                        )
                    } else {
                        DataState.data(
                            response = Response(
                                RESTORE_NOTE_FAILED
                                ,
                                UIComponentType.Toast(),
                                MessageType.Error()
                            ), data = null, stateEvent = stateEvent
                        )
                    }
                }
            }.getResult()
            emit(cacheResult)
            updateNetwork(cacheResult?.stateMessage?.response?.message, note)
        }

    private suspend fun updateNetwork(message: String?, note: Note){
        if(message.equals(RESTORE_NOTE_SUCCESS)){

            // insert into "notes" node
            safeApiCall(IO){
                noteNetworkDataSource.insertOrUpdateNote(note)
            }

            // remove from "deleted" node
            safeApiCall(IO){
                noteNetworkDataSource.deleteDeletedNote(note)
            }
        }
    }

    companion object {

        val RESTORE_NOTE_SUCCESS = "Successfully restored the deleted note."
        val RESTORE_NOTE_FAILED = "Failed to restore the deleted note."

    }
}