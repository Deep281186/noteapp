package com.example.notesapplication.business.interactors.notelist

import com.example.notesapplication.business.data.cache.CacheResponseHandler
import com.example.notesapplication.business.data.cache.abstraction.NoteCacheDataSource
import com.example.notesapplication.business.data.network.abstraction.NoteNetworkDataSource
import com.example.notesapplication.business.data.util.safeApiCall
import com.example.notesapplication.business.data.util.safeCacheCall
import com.example.notesapplication.business.domain.model.Note
import com.example.notesapplication.business.domain.state.*
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListViewState
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class DeleteMultipleNotes(
    val noteCacheDataSource: NoteCacheDataSource,
    val noteNetworkDataSource: NoteNetworkDataSource
) {

    var onDeleteError = false

    fun deleteNotes(
        noteList: List<Note>,
        stateEvent: StateEvent
    ): Flow<DataState<NoteListViewState>?> = flow {
        val successFullDeletNotes = ArrayList<Note>()
        for (note in noteList) {
            val cacheResponse = safeCacheCall(IO) {
                noteCacheDataSource.deleteNote(note.id)
            }
            val cacheResult =
                object : CacheResponseHandler<NoteListViewState, Int>(cacheResponse, stateEvent) {
                    override suspend fun handleSuccess(resultObj: Int): DataState<NoteListViewState>? {
                        if (resultObj < 0) {
                            onDeleteError = true
                        } else {
                            successFullDeletNotes.add(note)
                        }
                        return null
                    }
                }.getResult()

            // check for random errors
            if (cacheResult?.stateMessage?.response?.message
                    ?.contains(stateEvent.errorInfo()) == true
            ) {
                onDeleteError = true
            }
            if (onDeleteError == true) {
                emit(
                    DataState.data<NoteListViewState>(
                        response = Response(
                            message = DELETE_NOTES_ERRORS,
                            uiComponentType = UIComponentType.Dialog(),
                            messageType = MessageType.Success()
                        ),
                        data = null,
                        stateEvent = stateEvent
                    )
                )
            } else {
                emit(
                    DataState.data<NoteListViewState>(
                        response = Response(
                            message = DELETE_NOTES_SUCCESS,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Success()
                        ),
                        data = null,
                        stateEvent = stateEvent
                    )
                )
            }


        }
        updateNetwork(successFullDeletNotes)
    }

    private suspend fun updateNetwork(successfulDeletes: ArrayList<Note>) {
        for (note in successfulDeletes) {
            // delete from "notes" node
            safeApiCall(IO) {
                noteNetworkDataSource.deleteNote(note.id)
            }
            // insert into "deletes" node
            safeApiCall(IO) {
                noteNetworkDataSource.insertDeletedNote(note)
            }
        }
    }

    companion object {
        val DELETE_NOTES_SUCCESS = "Successfully deleted notes."
        val DELETE_NOTES_ERRORS =
            "Not all the notes you selected were deleted. There was some errors."
        val DELETE_NOTES_YOU_MUST_SELECT = "You haven't selected any notes to delete."
        val DELETE_NOTES_ARE_YOU_SURE = "Are you sure you want to delete these?"
    }
}