package com.example.notesapplication.business.data.cache.implementation

import com.example.notesapplication.business.data.cache.abstraction.NoteCacheDataSource
import com.example.notesapplication.business.domain.model.Note
import com.example.notesapplication.frameWork.datasource.cache.abstraction.NoteDAOService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NoteCacheDataSourceImpl @Inject constructor(private val noteDaoService:NoteDAOService):NoteCacheDataSource {

    override suspend fun insertNote(note: Note): Long {
        return noteDaoService.insertNote(note)
    }

    override suspend fun deleteNote(pk: String): Int {
        return noteDaoService.deleteNote(pk)
    }

    override suspend fun deleteNotes(notes: List<Note>): Int {
        return noteDaoService.deleteNotes(notes)
    }

    override suspend fun updateNote(pk: String, newTitle: String, body: String?,timeStamp:String?):Int {
        return noteDaoService.updateNote(pk, newTitle, body,timeStamp)
    }
    override suspend fun getAllNotes(): List<Note> {
        return noteDaoService.getAllNotes()
    }

    override suspend fun searchNotes(query: String, filterAndOrder: String, page: Int): List<Note> {
       return noteDaoService.returnOrderedQuery(query,filterAndOrder,page)
    }

    override suspend fun searchNotebyId(pk: String): Note? {
        return noteDaoService.searchNoteById(pk)
    }



    override suspend fun getnumNotes(): Int {
        return noteDaoService.getNumNotes()
    }

    override suspend fun insertNotes(notes: List<Note>): LongArray{
        return noteDaoService.insertNotes(notes)
    }
}