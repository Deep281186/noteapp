package com.example.notesapplication.business.interactors.notelist

import com.example.notesapplication.business.data.cache.CacheResponseHandler
import com.example.notesapplication.business.data.cache.abstraction.NoteCacheDataSource
import com.example.notesapplication.business.data.util.safeCacheCall
import com.example.notesapplication.business.domain.state.*
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListViewState
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GetNumNotes constructor(val noteCacheDataSource: NoteCacheDataSource) {

    fun getNumsOfNotes(stateEvent: StateEvent): Flow<DataState<NoteListViewState>?> = flow {

        val cacheResponse = safeCacheCall(IO) {
            noteCacheDataSource.getnumNotes()
        }
        val cacheResults =
            object : CacheResponseHandler<NoteListViewState, Int>(cacheResponse, stateEvent) {
                override suspend fun handleSuccess(resultObj: Int): DataState<NoteListViewState>? {

                    val viewState = NoteListViewState(numNotesInCache = resultObj)
                    return DataState.data(
                        response = Response(
                            message = GET_NUM_NOTES_SUCCESS,
                            messageType = MessageType.Success(),
                            uiComponentType = UIComponentType.None()
                        ), data = viewState, stateEvent = stateEvent
                    )
                }
            }.getResult()

        emit(cacheResults)
    }


    companion object {
        val GET_NUM_NOTES_SUCCESS = "Successfully retrieved the number of notes from the cache."
        val GET_NUM_NOTES_FAILED = "Failed to get the number of notes from the cache."
    }
}