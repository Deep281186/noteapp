package com.example.notesapplication.business.data.cache

object CacheConstants {
    const val CACHE_TIMEOUT = 2000L
}