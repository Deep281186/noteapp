package com.example.notesapplication.business.interactors.notelist

import com.example.notesapplication.business.interactors.common.DeleteNote
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListStateEvent
import com.example.notesapplication.frameWork.presentation.notelist.state.NoteListViewState
import javax.inject.Inject

class NoteListInteractors
@Inject
constructor(val insertNewNote: InsertNewNote,
             val deleteNote: DeleteNote<NoteListViewState>,
             val searchNotes: SearchNotes,
             val getNumNotes: GetNumNotes,
             val restoreDeletedNote: RestoreDeletedNote,
             val deleteMultipleNotes: DeleteMultipleNotes
) {
}