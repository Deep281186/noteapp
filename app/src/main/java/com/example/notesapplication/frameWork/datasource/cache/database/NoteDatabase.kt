package com.example.notesapplication.frameWork.datasource.cache.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.notesapplication.frameWork.datasource.cache.model.NoteCacheEntity

@Database(entities = [NoteCacheEntity::class ], version = 1)
abstract class NoteDatabase:RoomDatabase() {

    abstract fun noteDao(): NoteDAO

    companion object{
        val DATABASE_NAME: String = "note_db"
    }

}