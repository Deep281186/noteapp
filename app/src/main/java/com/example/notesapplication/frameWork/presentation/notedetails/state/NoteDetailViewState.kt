package com.example.notesapplication.frameWork.presentation.notedetails.state

import android.os.Parcelable
import com.example.notesapplication.business.domain.model.Note
import com.example.notesapplication.business.domain.state.ViewState
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NoteDetailViewState(
    var note: Note? = null,
    var isUpdatePending: Boolean? = null
) : Parcelable, ViewState