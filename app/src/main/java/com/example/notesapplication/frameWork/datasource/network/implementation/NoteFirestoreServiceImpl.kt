package com.example.notesapplication.frameWork.datasource.network.implementation

import com.example.notesapplication.business.domain.model.Note
import com.example.notesapplication.frameWork.datasource.network.abstraction.NoteFirestoreService
import com.example.notesapplication.frameWork.datasource.network.mapper.NetworkMapper
import com.example.notesapplication.frameWork.datasource.network.model.NoteNetworkEntity
import com.example.notesapplication.util.cLog
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.tasks.await
import java.lang.Exception
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NoteFirestoreServiceImpl
@Inject
constructor(
    private val firebaseAuth: FirebaseAuth,
    private val fireStore: FirebaseFirestore,
    private val networkMapper: NetworkMapper
) : NoteFirestoreService {
    override suspend fun insertOrUpdateNote(note: Note) {
        val entity = networkMapper.mapToEntity(note)
        entity.updated_at = Timestamp.now()
        fireStore.collection(NOTES_COLLECTION).document(USER_ID).collection(NOTES_COLLECTION)
            .document(entity.id)
            .set(entity)//Convert TASK to Suspend fun by calling .await(). Required Dependencies = kotlin_coroutines_play_services
            .addOnFailureListener {
                cLog(it.message)
            }
            .await()// required to convert this call to suspend fun in Coroutine// It will w8 util finish in Coroutine= Sync call

    }

    /**
     * Steps for Batch Insert/Update
     * 1> Set collectionRef=fireStore.collection(NOTES_COLLECTION).document(USER_ID).collection(NOTES_COLLECTION)
     * 2> Run batch = fireStore.runBatch { batch-> --code for update-- &&
     *      set     documentRef= collectionRef.document(networkEntity.id)
     *  then call   batch.set(documentRef, networkEntity)
     * }
     * */
    override suspend fun insertOrUpdateNotes(notes: List<Note>) {
        if (notes.size > 500) {
            throw Exception("Cannot insert notes more than 500 at once.")
        }
        val collectionRef =
            fireStore.collection(NOTES_COLLECTION).document(USER_ID).collection(NOTES_COLLECTION)
        fireStore.runBatch { batch ->
            for (note in notes) {
                val networkEntity = networkMapper.mapToEntity(note)
                networkEntity.updated_at = Timestamp.now()
                val documentRef = collectionRef.document(networkEntity.id)
                batch.set(documentRef, networkEntity)

            }
        }.addOnFailureListener {
            cLog(it.toString())
        }.await()


    }

    override suspend fun deleteNote(primaryKey: String) {
        fireStore.collection(NOTES_COLLECTION).document(USER_ID).collection(NOTES_COLLECTION)
            .document(primaryKey).delete().addOnFailureListener {
                cLog(it.toString())
            }.await()
    }

    override suspend fun insertDeletedNote(note: Note) {
        val entity = networkMapper.mapToEntity(note)
        fireStore.collection(DELETES_COLLECTION).document(USER_ID).collection(DELETES_COLLECTION)
            .document(entity.id)
            .set(entity)//Convert TASK to Suspend fun by calling .await(). Required Dependencies = kotlin_coroutines_play_services
            .addOnFailureListener {
                cLog(it.toString())
            }
            .await()// required to convert this call to suspend fun in Coroutine// It will w8 util finish in Coroutine= Sync call
    }

    override suspend fun insertDeletedNotes(notes: List<Note>) {
        if (notes.size > 500) {
            throw Exception("Cannot insert notes more than 500 at once.")
        }
        val collectionRef =
            fireStore.collection(DELETES_COLLECTION).document(USER_ID)
                .collection(DELETES_COLLECTION)
        fireStore.runBatch { batch ->
            for (note in notes) {
                val networkEntity = networkMapper.mapToEntity(note)
                val documentRef = collectionRef.document(networkEntity.id)
                batch.set(documentRef, networkEntity)
            }
        }.addOnFailureListener {
            cLog(it.toString())
        }.await()
    }

    override suspend fun deleteDeletedNote(note: Note) {
        fireStore.collection(DELETES_COLLECTION).document(USER_ID).collection(DELETES_COLLECTION)
            .document(note.id).delete().addOnFailureListener {
                cLog(it.toString())
            }.await()
    }

    override suspend fun deleteAllNotes() {
        fireStore.collection(DELETES_COLLECTION).document(USER_ID).delete().addOnFailureListener {
            cLog(it.toString())
        }
        fireStore.collection(NOTES_COLLECTION).document(USER_ID).delete().addOnFailureListener {
            cLog(it.toString())
        }
    }

    override suspend fun getDeletedNotes(): List<Note> {
        return networkMapper.entityListToNoteList(
            fireStore.collection(DELETES_COLLECTION).document(USER_ID)
                .collection(DELETES_COLLECTION).get().addOnFailureListener {
                    cLog(it.toString())
                }.await()
                .toObjects(NoteNetworkEntity::class.java)// Declaring expected object from the firestore response
        )
    }

    override suspend fun searchNote(note: Note): Note? {

        return fireStore.collection(NOTES_COLLECTION).document(USER_ID).collection(NOTES_COLLECTION)
            .document(note.id).get().addOnFailureListener {
                cLog(it.toString())
            }.await().toObject(NoteNetworkEntity::class.java)?.let {
                networkMapper.mapFromEntity(it)
            }

    }

    override suspend fun getAllNotes(): List<Note> {
        return networkMapper.entityListToNoteList(
            fireStore.collection(NOTES_COLLECTION).document(USER_ID).collection(NOTES_COLLECTION)
                .get().addOnFailureListener {
                    cLog(it.toString())
                }.await().toObjects(NoteNetworkEntity::class.java)
        )
    }

    companion object {
        const val NOTES_COLLECTION = "notes"
        const val USERS_COLLECTION = "users"
        const val DELETES_COLLECTION = "deletes"
        const val USER_ID = "J2T2nrdosHSvUKQfb06Qy5qJekx2" // hardcoded for single user
        const val EMAIL = "deep281186@gmail.com"
    }
}