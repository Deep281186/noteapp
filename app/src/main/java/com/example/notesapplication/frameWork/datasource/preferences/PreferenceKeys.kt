package com.example.notesapplication.frameWork.datasource.preferences

class PreferenceKeys {
    companion object{

        // Shared Preference Files:
        const val NOTE_PREFERENCES: String = "com.example.notesapplication"

        // Shared Preference Keys
        val NOTE_FILTER: String = "${NOTE_PREFERENCES}.NOTE_FILTER"
        val NOTE_ORDER: String = "${NOTE_PREFERENCES}.NOTE_ORDER"

    }
}