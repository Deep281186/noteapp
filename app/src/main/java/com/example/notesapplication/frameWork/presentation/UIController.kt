package com.example.notesapplication.frameWork.presentation

import com.example.notesapplication.business.domain.state.DialogInputCaptureCallback
import com.example.notesapplication.business.domain.state.Response
import com.example.notesapplication.business.domain.state.StateMessageCallback

interface UIController {

    fun displayProgressBar(isDisplayed: Boolean)

    fun hideSoftKeyboard()

    fun displayInputCaptureDialog(title: String, callback: DialogInputCaptureCallback)

    fun onResponseReceived(
        response: Response,
        stateMessageCallback: StateMessageCallback
    )

}