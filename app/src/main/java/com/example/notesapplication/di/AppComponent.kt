package com.example.notesapplication.di

import com.example.notesapplication.MainActivity
import com.example.notesapplication.frameWork.presentation.BaseApplication
import com.example.notesapplication.frameWork.presentation.notelist.NoteListFragment
import dagger.BindsInstance
import dagger.Component
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Singleton

@FlowPreview
@ExperimentalCoroutinesApi
@Singleton
@Component(modules = [AppModule::class, ProductionModule::class,NoteViewModelModule::class])
interface AppComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance app: BaseApplication): AppComponent
    }

    fun inject(mainActivity: MainActivity)
    fun inject(noteListFragment: NoteListFragment)
}